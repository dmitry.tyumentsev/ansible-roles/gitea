Role Name
=========

Installs and configures [Gitea](https://about.gitea.com/) server on Fedora, Debian/Ubuntu or AltLinux servers.

Example Playbook
----------------
```yml
- hosts: gitea
  become: true
  roles:
    - tyumentsev4.gitea
```
